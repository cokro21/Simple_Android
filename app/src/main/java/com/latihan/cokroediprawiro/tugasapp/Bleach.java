package com.latihan.cokroediprawiro.tugasapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class Bleach extends AppCompatActivity {

    private static Button Bclose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bleach);

        Bclose = (Button)findViewById(R.id.Bclose);

        Bclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCloseCliked();
            }
        });
    }

    private void onCloseCliked()
    {
        Log.d(AppSettings.tag,"onCloseCliked");

        finish();
    }
}
