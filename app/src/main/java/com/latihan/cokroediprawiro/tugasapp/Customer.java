package com.latihan.cokroediprawiro.tugasapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class Customer extends AppCompatActivity {

    private static Button BtnClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer);

        BtnClose = (Button)findViewById(R.id.BtnClose);

        BtnClose.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                onCloseClicked();
            }
        });
    }

    private void onCloseClicked()
    {
        Log.d(AppSettings.tag,"onCloseClicked");

        finish();
    }
}
