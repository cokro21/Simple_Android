package com.latihan.cokroediprawiro.tugasapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class Fairy extends AppCompatActivity {

    private static Button Ftail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fairy);

        Ftail = (Button)findViewById(R.id.Ftail);

        Ftail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCloseCliked();
            }
        });
    }

    private void onCloseCliked()
    {
        Log.d(AppSettings.tag,"onCloseCliked");

        finish();
    }
}
