package com.latihan.cokroediprawiro.tugasapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class AppTugas extends AppCompatActivity {

    private static Button BtnStart;
    private static Button BtnResult;
    private static Button BtnBleach;
    private static Button BtnTail;
    private static final int feedCustomer = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_tugas);

        BtnStart = (Button)findViewById(R.id.BtnStart);
        BtnResult = (Button)findViewById(R.id.BtnResult);
        BtnBleach = (Button)findViewById(R.id.BtnBleach);
        BtnTail = (Button)findViewById(R.id.BtnTail);

        BtnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onStartClicked();
            }
        });

        BtnResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onResultClicked();
            }
        });

        BtnBleach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 Intent bleach1 = new Intent(getApplicationContext(),Bleach.class);
                 startActivity(bleach1);
            }
        });

        BtnTail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Fairy = new Intent(getApplicationContext(),Fairy.class);
                startActivity(Fairy);
            }
        });
    }

    private void onStartClicked()
    {
        Log.d(AppSettings.tag,"onStartClicked");

        Intent intent = new Intent("android.intent.action.Customer");
        startActivity(intent);
    }

    private void onResultClicked()
    {
        Log.d(AppSettings.tag,"onResultClicked");
        Intent intent = new Intent("android.intent.action.customer2");
        startActivityForResult(intent,feedCustomer);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.d(AppSettings.tag,"onActivityResult");
    }
}
