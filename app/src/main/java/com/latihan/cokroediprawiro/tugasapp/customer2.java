package com.latihan.cokroediprawiro.tugasapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class customer2 extends AppCompatActivity {

    private static Button BtnClose1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer2);

        BtnClose1 = (Button)findViewById(R.id.BtnClose1);

        BtnClose1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCloseCliked();
            }
        });
    }

    private void onCloseCliked()
    {
        Log.d(AppSettings.tag, "onCloseClicked");

        finish();
    }
}
